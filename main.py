#! /usr/bin/python

import serial
import time

from time import sleep
from firebase import firebase

bluetoothSerial = serial.Serial( "/dev/rfcomm0", baudrate=9600 )
firebase = firebase.FirebaseApplication('https://smartfridge-a3daa.firebaseio.com', None)

count = None
while True:
    try:
        count = int(bluetoothSerial.readline())
        print count
        if count == 111:
            print "Reading Weight1"
            count2 = float(bluetoothSerial.readline())
            ts = time.time()
            result1 = firebase.patch('/Shelves/shelf0', data={"currentWeight":count2}, params={'print': 'pretty'})
            result2 = firebase.post('/WeightLogs/shelf0', data={"currentWeight":count2, "timestamp": ts}, params={'print': 'pretty'})
            print result1
            print result2
        elif count == 112:
            print "Reading Weight2"
            count2 = float(bluetoothSerial.readline())
            
            ts = time.time()
            result = firebase.patch('/Shelves/shelf1', data={"currentWeight":count2}, params={'print': 'pretty'})
            result2 = firebase.post('/WeightLogs/shelf1', data={"currentWeight":count2, "timestamp": ts}, params={'print': 'pretty'})
            print result1
            print result2
        elif count == 222:
            print "Reading Temperature"
            count2 = float(bluetoothSerial.readline())
            ts = time.time()
            
            result = firebase.post('/Temperature/', data={"temp":count2, "timestamp":ts}, params={'print': 'pretty'})
            print result
        
        elif count == 333:
            print "Reading Humidity"
            count2 = float(bluetoothSerial.readline())
            ts = time.time()
            
            result = firebase.post('/Humidity/', data={"hum":count2, "timestamp":ts}, params={'print': 'pretty'})
            print result
        elif count == 444:
            print "Color Detected as "
            count2 = bluetoothSerial.readline()
            print count2
except:
    pass    # Ignore any errors that may occur and try again

